// Package settings provides a setting system with any parameters necessary to configure the runtime.
//
// All thoses parameters can be changed with flags when the binray is running.
package settings

import (
	"flag"
	"fmt"
	"os"
	"strconv"
)

var (
	Color                 bool
	Verbose               uint
	Port                  string
	PathToSharedDirectory string
	Security              bool
	OpenCache             bool
	HtmlCache             bool

	port uint
)

// To parse the flags in the command line and apply thoses settings to the runtime.
func ParseFlags() {
	flag.BoolVar(&Color, `c`, true, `enable colors in logs`)
	flag.UintVar(&Verbose, `v`, 3, `the verbosity of the logs to use (0 to 5)`)
	flag.StringVar(&PathToSharedDirectory, `x`, ``, `the path to the directory to share (mandatory)`)
	flag.UintVar(&port, `p`, 55555, `the port to serve on`)
	flag.BoolVar(&Security, `s`, false, `enable Transport Layer Security aka TLS`)
	flag.BoolVar(&OpenCache, `f`, true, `enable the cache mecanism for open directories (file descriptor are not closed)`)
	flag.BoolVar(&HtmlCache, `d`, true, `enable the cache mecanism for HTML pages (generation is stored in RAM)`)
	flag.Parse()

	checkSettings()
}

// To check the validity of the settings.
func checkSettings() {
	if len(PathToSharedDirectory) == 0 {
		printError(`missing mandatory flag`)
	}

	file, err := os.Open(PathToSharedDirectory)
	if err != nil {
		printError(err.Error())
	}
	file.Close()

	Port = strconv.FormatUint(uint64(port), 10)
}

// To print an error, the defaults values message and exit.
func printError(message string) {
	fmt.Println("error: " + message + "\n\nUsage:")
	flag.PrintDefaults()
	os.Exit(1)
}
