// Package logs is a wrapper to a log system.
//
// The wrapper allows to easily change the dependency if needed.
package logs

import (
	"gitlab.com/AeN_555/colorlog"
)

// To configure the logger with the color and the log level.
func Configure(colorIsEnabled bool, level uint) {
	colorlog.SetColor(colorIsEnabled)
	colorlog.SetLevel(level)
}

// To display a debug log (level >= 5).
func Debug(msg ...any) {
	colorlog.Debug(msg...)
}

// To display a verbose log (level >= 4).
func Verbose(msg ...any) {
	colorlog.Verbose(msg...)
}

// To display an information log (level >= 3).
func Information(msg ...any) {
	colorlog.Information(msg...)
}

// To display a warning log (level >= 2).
func Warning(msg ...any) {
	colorlog.Warning(msg...)
}

// To display an error log (level >= 1).
func Error(err error, msg ...any) {
	colorlog.Error(err, msg...)
}
