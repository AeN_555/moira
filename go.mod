module gitlab.com/AeN_555/moira

go 1.22.3

require gitlab.com/AeN_555/colorlog v1.2.0

require gitlab.com/AeN_555/termcolor v1.0.0 // indirect
