package security

import (
	"os"
	"os/exec"

	"gitlab.com/AeN_555/moira/logs"
)

const (
	moiraDirectory = `moira`
)

// To get the paths to private and the public keys.
//
// If the path to the configuration directory doesn't exist, it will be created and files will be generated.
// Otherwise, files will be used and nothing will be modified.
func KeysPaths() (privateKeyPath string, publicKeyPath string) {
	path, needed := createDirectoryIfNeeded()
	privateKeyPath = path + string(os.PathSeparator) + `key.pem`
	publicKeyPath = path + string(os.PathSeparator) + `cert.pem`
	if needed == true {
		cmd := exec.Command(`openssl`,
			`req`, `-x509`, `-nodes`,
			`-newkey`, `ec`,
			`-pkeyopt`, `ec_paramgen_curve:secp384r1`,
			`-keyout`, privateKeyPath,
			`-out`, publicKeyPath,
			`-days`, `3650`,
			`-subj`, `/`)
		err := cmd.Run()
		if err != nil {
			logs.Error(err, `failed to run command "`+cmd.String()+`" for generating keys`)
		}

		logs.Information(`security keys in "` + path + `" are generated and used`)
	} else {
		logs.Information(`security keys in "` + path + `" are used`)
	}

	return
}

// To check if the directory is already created, otherwise create it.
func createDirectoryIfNeeded() (directoryPath string, needed bool) {
	configPath, err := os.UserConfigDir()
	if err != nil {
		logs.Error(err, `failed to get user home directory`)
	}

	needed = false
	directoryPath = configPath + string(os.PathSeparator) + moiraDirectory
	if _, err := os.Stat(directoryPath); os.IsNotExist(err) == true {
		if err = os.MkdirAll(directoryPath, 0755); err != nil {
			logs.Error(err, `failed to create directory "`+directoryPath+`"`)
		}
		needed = true
	}

	return directoryPath, needed
}
