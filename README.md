# Moïra

![Logo](./logo.png "Moïra logo")

A Go program to serve HTTP client allowing them to download server files.

Moïra is a HTTP server that allow each client to download files in the server directory.  
It's a simple and small program to allow anyone to share anything with anyone.

### Requierements

The package depends on [ColorLog](https://gitlab.com/AeN_555/colorlog "Link to ColorLog").

Obviously, it also requires [Go](https://golang.org/ "Link to Golang") itself.


### Installation

Moïra can be installed with the Go command:

```bash
$ go install gitlab.com/AeN_555/moira@latest
```

### Example

*For the server side:*

```bash
$ ls -R /srv
/srv/:
work otherFile.txt

/srv/work/:
pictures archive.zip document.pdf

/srv/work/pictures:
image.png
$
$ moira -x "/srv"
$ 2024/07/17 20:30:06  I  server shares path "/srv" on port 55555
```

*For the client side:*

Just open a browser and connect on the server with address "${myPublicIp}:55555".
Result looks like:

![Result of the example](./result.png)

### Usage

```
Usage of ./moira:
  -c    enable color in logs (default true)
  -d    enable the cache mecanism for HTML pages (generation is stored in RAM) (default true)
  -f    enable the cache mecanism for open directories (file descriptor are not closed) (default true)
  -p uint
        the port to serve on (default 55555)
  -s    enable Transport Layer Security aka TLS
  -v uint
        the verbosity of the logs to use (0 to 5) (default 3)
  -x string
        the path to the directory to share (mandatory)
```

### Knowned issues

 - *For the client, the browser says the address is unreachable.*

Common mistake is to not open the port of your ISP router.  
Most of the time, you connect on your router with your browser, you open the TCP port for your computer and its works.

 - *In preview mode, some files are downloaded not previewed.*

Yep sorry, the fix is coming someday.

 - *In secure mode (-s), web browser says the certificate is invalide.*

Yep, I'm not sure if it's the server or the generated certificates.  
Maybe it's because the certificates are self-signed ... I'm not sure.

 - *The server takes a lots of ressources (RAM and/or file descriptors).*

By default, 2 cache systems are enabled.  
One keeps the file descriptors of the directories open to avoid Syscall/IO.  
An other keeps the HTML generation in RAM.  
If you want to sacrifice performance over ressources you should deactivate thoses caches (one or both).
