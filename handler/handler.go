// Package handler provides a HTTP handler to allow path exploration and file downloading.
//
// This handler allows to explore a path if a folder is requested or send the selected file.
package handler

import (
	"errors"
	"io"
	"net/http"
	"net/url"
	"os"
	"path"

	"gitlab.com/AeN_555/moira/handler/cache"
	"gitlab.com/AeN_555/moira/handler/common"
	"gitlab.com/AeN_555/moira/handler/download"
	"gitlab.com/AeN_555/moira/handler/preview"
	"gitlab.com/AeN_555/moira/logs"
	"gitlab.com/AeN_555/moira/settings"
)

type (
	Handler struct {
		openCache cache.OpenCache
		htmlCache cache.HtmlCache
		preview   preview.Preview
		download  download.Download
	}
)

// To create the Moïra handler of the server.
//
// This handler handles every requestes : directory browsing, file previewing and file sending.
func NewHandler() *Handler {
	h := &Handler{
		openCache: *cache.NewOpenCache(),
		htmlCache: *cache.NewHtmlCache(),
		preview:   *preview.NewPreview(),
		download:  *download.NewDownload(),
	}
	h.openCache.SetDoCallback(openFile)
	h.openCache.SetUndoCallback(func(value cache.OpenCacheValue) error { return value.File.Close() })
	h.htmlCache.SetDoCallback(h.preview.GenerateHtml)
	h.preview.SetOpenCache(&h.openCache)
	h.download.SetOpenCache(&h.openCache)
	return h
}

// To handle a request from client.
func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	logs.Information(`receiving request "` + r.URL.String() + `" from "` + r.RemoteAddr + `"`)

	realPath, err := url.PathUnescape(r.URL.Path)
	if err != nil {
		logs.Warning(`cannot satisfy requested file: ` + err.Error())
		common.DisplayCannotShare(err, w, r)
		return
	}

	var v cache.OpenCacheValue
	if settings.OpenCache == true {
		v, err = h.openCache.Get(realPath)
	} else {
		v, err = h.openCache.Do(realPath)
	}
	if err != nil && errors.Is(err, cache.NotStoredError) == false {
		logs.Warning(`cannot satisfy requested file: ` + err.Error())
		common.DisplayCannotShare(err, w, r)
		return
	}
	if settings.OpenCache == false {
		defer func() {
			err := h.openCache.Undo(v)
			if err != nil {
				logs.Error(err, `fail to release ressource`)
			}
		}()
	}

	if common.DownloadIsRequested(r) == true {
		if v.FileInfo.IsDir() == true {
			h.download.SendDirectory(v, w, r)
		} else {
			h.download.SendFile(v, w, r)
		}
	} else {
		if v.FileInfo.IsDir() == true {
			h.displayDirectory(v, w, r)
		} else {
			logs.Verbose(`displaying file "` + r.URL.Path + `" to "` + r.RemoteAddr + `"`)
			http.ServeFile(w, r, settings.PathToSharedDirectory+realPath)
			logs.Verbose(`end displaying file "` + r.URL.Path + `" to "` + r.RemoteAddr + `"`)
		}
	}

	logs.Information(`end receiving request "` + r.URL.String() + `" from "` + r.RemoteAddr + `"`)
}

// To clear the handler and its caches.
func (h *Handler) Clear() {
	errs := h.openCache.Clear()
	for _, err := range errs {
		logs.Warning(`failed to clean cached file: ` + err.Error())
	}

	h.htmlCache.Clear()
}

// To display a HTML web page listing all available directories and files.
//
// It will use the cache system if set.
func (h *Handler) displayDirectory(value cache.OpenCacheValue, w http.ResponseWriter, r *http.Request) {
	logs.Verbose(`displaying directory "` + value.File.Name() + `" to "` + r.RemoteAddr + `"`)

	s := ""
	k := cache.HtmlCacheKey{Directory: value}
	if settings.HtmlCache == true {
		s = h.htmlCache.Get(k)
	} else {
		s = h.preview.GenerateHtml(k)
	}
	// send the HTML page
	io.WriteString(w, s)

	logs.Verbose(`end displaying directory "` + value.File.Name() + `" to "` + r.RemoteAddr + `"`)
}

// To open a file from the the root of the shared directory.
//
// If the file ends up with '/', the last '/' will be discared.
// This behaviour is mainly choosen to avoid multiple values of the same file in the cache.
// Moreover, it allows more robustness for the rest of the code : it follows path-clean mecanism from standard library.
//
// If the opened file is not a directory, it's not stored in the cache system because it's not efficient.
// An error will be set to warn but the result value will be ok to use.
//
// In details : sharing the same file across users will result to truncacted files because the same seeker is used.
// To fix that, it's possible to lock the file and make the next client wait.
// Once the previous client is finished, reset the seeker and unlock the next client.
// But at this point, a bottleneck is created and the IO efficiency is lost :
// for each file, instead of open/close it will lock/seek/unlock.
// The problem doesn't apply to directories.
func openFile(pathToFile string) (value cache.OpenCacheValue, err error) {
	p := path.Clean(settings.PathToSharedDirectory + pathToFile)
	logs.Verbose(`opening file "` + p + `"`)
	value.File, err = os.Open(p)
	if err != nil {
		return
	}

	value.FileInfo, err = value.File.Stat()
	if err != nil {
		value.File.Close()
		value.File = nil
		return
	}

	if value.FileInfo.IsDir() == true {
		value.DirectoryInfos, err = value.File.ReadDir(0)
		if err != nil {
			logs.Verbose(`cannot read directory` + err.Error())
			value.DirectoryInfos = nil
			err = nil
		}
	} else {
		err = cache.NotStoredError
	}
	return
}
