// Package preview provides the preview feature.
//
// Preview feature is used to explore a directory and open a file in a browser.
// This package allows to generate HTML string that provides this feature.
package preview

import (
	"bytes"
	"cmp"
	"fmt"
	"html/template"
	"sort"
	"strings"
	"time"

	"gitlab.com/AeN_555/moira/handler/cache"
	"gitlab.com/AeN_555/moira/handler/common"
	"gitlab.com/AeN_555/moira/logs"
	"gitlab.com/AeN_555/moira/settings"
)

type (
	Preview struct {
		openCache     *cache.OpenCache
		htmlGenerator template.Template
	}

	data struct {
		CurrentPath []directoryData
		Files       []fileData
	}
	directoryData struct {
		Url     string
		Text    string
		Enabled string
	}
	fileData struct {
		Url  string
		Text string
		Size string
		Date string
	}
)

const (
	css = `		<style>
			tr > th {
				padding: 5px;
				background-color: rgb(225, 225, 225);
			}

			tr > td {
				padding: 3px;
			}

			tr:nth-of-type(even) > td {
				background-color: rgb(250, 250, 250);
			}
			tr:nth-of-type(odd) > td {
				background-color: rgb(240, 240, 240);
			}
			tr:hover > td {
				box-shadow: inset -2px -2px 15px rgba(0, 100, 250, 0.1),
				            inset  2px  2px 15px rgba(0, 100, 250, 0.1);
			}

			tr > td:nth-of-type(1), tr > td:nth-of-type(3), tr > td:nth-of-type(4) {
				text-align: center;
				vertical-align: middle;
			}

			.downloadButton {
				padding: 0px 5px;

				/* border */
				border-color: rgb(0, 0, 0);
				border-width: 1px;
				border-radius: 40%;

				/* background */
				background-color: rgb(240, 240, 240);
				box-shadow: inset -3px -3px 5px rgba(0, 0, 0, 0.2);

				/* text */
				color: #000;
				font-size: 150%;
				line-height: 1;
				text-align: center;
			}
			.downloadButton:hover {
				background-color: rgb(220, 220, 220);
				cursor: pointer;
			}
			.downloadButton:active {
				background-color: rgb(250, 250, 250);
			}
		</style>

`
	tpl = `		<div>
{{range .CurrentPath}}			<form style="display: inline-block" action="{{.Url}}">
				<input type="submit" value="{{.Text}}" {{.Enabled}}>
			</form>
{{end}}		</div>

		<div style="width: 100%">
			<br>
			<table style="width: 90%; margin: 0 auto">
				<tr>
					<th style="width: 5%"></th>
					<th style="width: 70%">File name</th>
					<th style="width: 10%">Size</th>
					<th style="width: 15%">Last modification date</th>
				</tr>
{{range .Files}}				<tr>
					<td><form action="{{.Url}}"><button type="submit" name="download" title="Click to download" class="downloadButton">⭳</button></form></td>
					<td><a href="{{.Url}}">{{.Text}}</a></td>
					<td>{{.Size}}</td>
					<td>{{.Date}}</td>
				</tr>
{{end}}			</table>
		</div>
`
)

// To create a preview HTML generator.
func NewPreview() *Preview {
	p, err := template.New(`preview`).Parse(tpl)
	if err != nil {
		logs.Error(err, `cannot parse template for preview`)
	}
	return &Preview{htmlGenerator: *p}
}

// To set the open cache if available.
func (p *Preview) SetOpenCache(openCache *cache.OpenCache) {
	p.openCache = openCache
}

// To generate a HTML web page listing all available directories and files.
//
// The server will read the path and make an entry for each file in the HTML web page.
// Each directory and file will be an hyperlink in the HTML page.
// Each displayed directory will have '/' at the end of the name to indicate it's a directory.
func (p *Preview) GenerateHtml(key cache.HtmlCacheKey) string {
	directoryPath := key.Directory.File.Name()
	logs.Verbose(`generating HTML string for directory "` + directoryPath + `"`)

	path := common.RelativePath(directoryPath, settings.PathToSharedDirectory,
		`cannot generate HTML string to preview directory`) + `/`

	d := data{
		CurrentPath: directoriesData(path),
		Files:       p.filesData(key.Directory, path),
	}

	var b bytes.Buffer
	if err := p.htmlGenerator.Execute(&b, d); err != nil {
		logs.Error(err, `cannot execute template for preview`)
	}

	logs.Verbose(`end generating HTML string for directory "` + directoryPath + `"`)
	return common.HtmlBegin + css + b.String() + common.HtmlEnd
}

// To create template data representing all directories to the current path.
func directoriesData(currentPath string) (res []directoryData) {
	res = append(res, directoryData{Url: `/`, Text: `/`})
	for k, v := range strings.Split(currentPath, `/`) {
		if k == 0 { // drop the empty string before the first '/'
			continue
		}
		if len(v) == 0 { // drop the empty string after the last '/'
			break
		}
		res = append(res, directoryData{Url: res[k-1].Url + v + `/`, Text: v})
	}
	res[len(res)-1].Enabled = `disabled`
	return res
}

// To create template data representing all files from a directory.
func (p *Preview) filesData(value cache.OpenCacheValue, currentPath string) (res []fileData) {
	entries := value.DirectoryInfos
	// sort entries to directories then files and by alphabetical order
	sort.Slice(entries, func(i, j int) bool {
		a := entries[i]
		b := entries[j]
		if a.IsDir() != b.IsDir() {
			return a.IsDir()
		}
		return cmp.Less(a.Name(), b.Name())
	})

	res = make([]fileData, len(entries))
	for k, entry := range entries {
		fileName := entry.Name()
		icon := `🗀 `
		size := `unknown`
		date := `unknown`

		fi, err := entry.Info()
		if err != nil {
			logs.Verbose(`cannot compute file informations: ` + err.Error())
		} else {
			date = fi.ModTime().Format(time.DateTime)
		}

		if entry.IsDir() {
			path := currentPath + fileName
			fileName = fileName + `/`
			icon = `🖿 `

			var err error
			v := cache.OpenCacheValue{}
			if settings.OpenCache == true {
				v, err = p.openCache.Get(path)
			} else {
				v, err = p.openCache.Do(path)
			}
			if err != nil {
				logs.Verbose(`cannot compute content of directory: ` + err.Error())
			} else {
				if settings.OpenCache == false {
					defer func() {
						err = p.openCache.Undo(v)
						if err != nil {
							logs.Error(err, `fail to release ressource`)
						}
					}()
				}
				if v.DirectoryInfos == nil {
					logs.Verbose(err, `cannot read directory "`+value.File.Name()+`"`)
				} else {
					size = fmt.Sprintf(`%d files`, len(v.DirectoryInfos))
				}
			}
		} else {
			if fi != nil {
				size = humanReadableSize(fi.Size())
			}
		}

		res[k] = fileData{
			Url:  currentPath + fileName,
			Text: icon + fileName,
			Size: size,
			Date: date,
		}
	}
	return res
}

// To get a human readable size of a number of octets.
func humanReadableSize(sizeInOctet int64) string {
	const unit = 1000

	if sizeInOctet < unit {
		return fmt.Sprintf("%d o", sizeInOctet)
	}

	div := int64(unit)
	exp := 0
	for c := sizeInOctet / unit; c >= unit; c = c / unit {
		div = div * unit
		exp++
	}
	return fmt.Sprintf("%.1f %co", float64(sizeInOctet)/float64(div), "kMGTPE"[exp])
}
