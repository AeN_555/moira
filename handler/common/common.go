// Package commmon provides common tools among the handler, the preview and the download.
package common

import (
	"fmt"
	"io"
	"net/http"
	"strings"

	"gitlab.com/AeN_555/moira/logs"
)

const (
	HtmlBegin = `<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
	</head>
	<body>
`
	HtmlEnd = `	</body>
	<footer>
		<center>
			<hr />
			<a href="https://gitlab.com/AeN_555/moira" target="_blank">
				<img src="https://gitlab.com/AeN_555/moira/-/raw/master/logo.png" alt="Moïra logo" width="10%"/>
			</a>
			<br>
			<a href="https://gitlab.com/AeN_555/moira" target="_blank">Powered by Moïra</a>
		</center>
	</footer>
</html>`

	downloadKey = `download`
)

func RelativePath(filePath string, fullPath string, message string) string {
	path, found := strings.CutPrefix(filePath, fullPath)
	if found == false {
		logs.Error(fmt.Errorf(`file %q is not in path %q`, filePath, fullPath), message)
	}
	return path
}

// To know if a download request is present.
func DownloadIsRequested(r *http.Request) bool {
	return (r.Method == `GET`) && (r.URL.Query().Has(downloadKey) == true)
}

// To generate a HTML web page for the error.
//
// If the server cannot read a file, directory or any unavailable path, it will generate this HTML web page.
// It's like a "403 forbidden", "404 not found" and other common error in one dynamic page.
func DisplayCannotShare(e error, w http.ResponseWriter, r *http.Request) {
	logs.Verbose(`displaying error for request "` + r.URL.Path + `" to "` + r.RemoteAddr + `"`)

	// send the HTML page
	io.WriteString(w, HtmlBegin+
		"\t\t<h1><u>Error:</u> unable to share \""+r.URL.Path+"\"</h1>\n"+
		"\t\t<p><u>Reason:</u> "+e.Error()+"</p>\n"+
		HtmlEnd)

	logs.Verbose(`end displaying error for request "` + r.URL.Path + `" to "` + r.RemoteAddr + `"`)
}
