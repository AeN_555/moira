// Package download provides the download feature.
//
// Download feature is used to send a file from the server to a client.
package download

import (
	"archive/tar"
	"io"
	"net/http"
	"path"
	"strconv"

	"gitlab.com/AeN_555/moira/handler/cache"
	"gitlab.com/AeN_555/moira/handler/common"
	"gitlab.com/AeN_555/moira/logs"
	"gitlab.com/AeN_555/moira/settings"
)

type (
	Download struct {
		openCache *cache.OpenCache
	}
)

// To create a download generator.
func NewDownload() *Download {
	return &Download{}
}

// To set the open cache if available.
func (d *Download) SetOpenCache(openCache *cache.OpenCache) {
	d.openCache = openCache
}

// To send the requested file to the client.
//
// The request is just the-server-url + '/' + the-requested-file + "?download" like "localhost/foo.txt?download".
// So the server will look for the-execution-path + '/' + the-requested-file like "/srv/foo.txt" and send it.
func (d *Download) SendFile(value cache.OpenCacheValue, w http.ResponseWriter, r *http.Request) {
	logs.Verbose(`sending file "` + value.File.Name() + `" to "` + r.RemoteAddr + `"`)

	// prepare the receiver to download the file
	filename := path.Base(value.File.Name())
	w.Header().Set("Content-Disposition", "attachment; filename="+filename)
	w.Header().Set("Content-Type", r.Header.Get("Content-Type"))
	w.Header().Set("Content-Length", strconv.Itoa(int(value.FileInfo.Size())))

	// send the file
	io.Copy(w, value.File)

	logs.Verbose(`end sending file "` + value.File.Name() + `" to "` + r.RemoteAddr + `"`)
}

// To send the requested directory to the client.
//
// The request is just the-server-url + '/' + the-requested-directory + "?download" like "localhost/foo?download".
// So the server will look for the-execution-path + '/' + the-requested-directory like "/srv/foo" to archvie it (with tar command) and send it.
func (d *Download) SendDirectory(value cache.OpenCacheValue, w http.ResponseWriter, r *http.Request) {
	logs.Verbose(`sending directory "` + value.File.Name() + `" to "` + r.RemoteAddr + `"`)

	p := common.RelativePath(value.File.Name(), settings.PathToSharedDirectory,
		`cannot send archive of the directory`)

	// prepare the receiver to download the file
	filename := path.Base(p)
	w.Header().Set("Content-Disposition", "attachment; filename="+filename+".tar")
	w.Header().Set("Content-Type", r.Header.Get("Content-Type"))
	w.Header().Set("Content-Length", r.Header.Get("Content-Length"))

	tw := tar.NewWriter(w)
	if err := tw.AddFS(cache.NewFS(d.openCache, settings.OpenCache, p)); err != nil {
		logs.Warning(`fail to send archive for directory "`, value.File.Name(), `": `, err.Error())
	}
	tw.Close()

	logs.Verbose(`end sending directory "` + value.File.Name() + `" to "` + r.RemoteAddr + `"`)
}
