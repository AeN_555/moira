package cache

import (
	"errors"
	"io/fs"
	"os"
	"sync"
)

type (
	OpenCache struct {
		data         map[string]OpenCacheValue
		doCallback   func(string) (OpenCacheValue, error)
		undoCallback func(OpenCacheValue) error
		mutex        sync.Mutex
	}

	OpenCacheValue struct {
		File           *os.File
		FileInfo       os.FileInfo
		DirectoryInfos []fs.DirEntry
	}
)

var (
	NotStoredError error = errors.New(`new data is not stored in cache`)
)

// To create a cache for all directory [os.File] and its [os.FileInfo] regarding a path to the directory.
//
// Every file can be opened with this cache but only the directory will be stored.
func NewOpenCache() *OpenCache {
	return &OpenCache{
		data:  make(map[string]OpenCacheValue),
		mutex: sync.Mutex{},
	}
}

// To set the callback that will do the job and will fill the cache.
//
// This callback will be called when the path to the file is not in the cache and so the [OpenCacheValue] has to be computed.
func (c *OpenCache) SetDoCallback(doCallback func(string) (OpenCacheValue, error)) {
	c.doCallback = doCallback
}

// To set the callback that will undo the job when the cache will be cleared.
//
// This callback will be called when the cache will be cleared.
func (c *OpenCache) SetUndoCallback(undoCallback func(OpenCacheValue) error) {
	c.undoCallback = undoCallback
}

// To manually call the do callback without use the cache.
func (c *OpenCache) Do(pathToFile string) (OpenCacheValue, error) {
	return c.doCallback(pathToFile)
}

// To manually call the undo callback without use the cache.
func (c *OpenCache) Undo(file OpenCacheValue) error {
	return c.undoCallback(file)
}

// To get the [OpenCacheValue] regarding the path to the file.
//
// If the path to the file is in the cache, the [OpenCacheValue] in the cache will be returned without computing.
// Otherwise, the callback set by [OpenCache.SetFillCallback] will be called and the result will be returned.
// If the result contains an error, the data will not be stored but returned with its error.
func (c *OpenCache) Get(pathToFile string) (value OpenCacheValue, err error) {
	c.mutex.Lock()
	defer c.mutex.Unlock()

	value, ok := c.data[pathToFile]
	if ok == false {
		value, err = c.doCallback(pathToFile)
		if err == nil {
			c.data[pathToFile] = value
		}
	}
	return value, err
}

// To clear the cache.
//
// The callback set by [OpenCache.SetCleanCallback] is used on every files.
func (c *OpenCache) Clear() (allErrors []error) {
	for _, f := range c.data {
		err := c.undoCallback(f)
		if err != nil {
			allErrors = append(allErrors, err)
		}
	}
	clear(c.data)
	return
}
