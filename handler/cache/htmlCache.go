// Package cache provides cache mecanisms.
//
// It contains 2 differents caches :
// - one about HTML string regarding a [HtmlCacheKey]
// - one about [FileCacheValue] regarding a path to the file
package cache

import (
	"sync"
)

type (
	HtmlCache struct {
		data       map[string]HtmlCacheKey
		doCallback func(HtmlCacheKey) string
		mutex      sync.Mutex
	}

	HtmlCacheKey struct {
		Directory OpenCacheValue
		result    string
	}
)

// To create a cache for HTML strings regarding a [HtmlCacheKey].
func NewHtmlCache() *HtmlCache {
	return &HtmlCache{
		data:  make(map[string]HtmlCacheKey),
		mutex: sync.Mutex{},
	}
}

// To set the callback that will do the job and will fill the cache.
//
// This callback will be called when the hash of the [HtmlCacheKey]
// (calculated by the callback set by [HtmlCache.SetHashCallback]) is not in the cache
// and so the HTML string has to be computed.
func (c *HtmlCache) SetDoCallback(doCallback func(HtmlCacheKey) string) {
	c.doCallback = doCallback
}

// To get the HTML string regarding the [HtmlCacheKey].
//
// If the hash of [HtmlCacheKey] (calculated by the callback set by [HtmlCache.SetHashCallback]) is in the cache,
// the HTML string in the cache will be returned without computing.
// Otherwise, the callback set by [HtmlCache.SetDoCallback] will be called and the result will be returned.
func (c *HtmlCache) Get(key HtmlCacheKey) (html string) {
	c.mutex.Lock()
	defer c.mutex.Unlock()

	p := key.Directory.File.Name()
	k, ok := c.data[p]
	if ok == false {
		k = key
		k.result = c.doCallback(key)
		c.data[p] = k
	}
	return k.result
}

// To clear the cache.
func (c *HtmlCache) Clear() {
	clear(c.data)
}
