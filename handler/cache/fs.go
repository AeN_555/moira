package cache

import (
	"errors"
	"io/fs"
	"os"
	"path"

	"gitlab.com/AeN_555/moira/logs"
)

type (
	cachedFS struct {
		openCache    *OpenCache
		cacheEnabled bool
		relativePath string
	}

	cachedFSFile struct {
		fs             *cachedFS
		openCacheValue OpenCacheValue
	}
)

// To create a FileSytem using cache system.
func NewFS(openCache *OpenCache, cacheEnabled bool, relativePath string) fs.FS {
	return &cachedFS{
		openCache:    openCache,
		cacheEnabled: cacheEnabled,
		relativePath: relativePath,
	}
}

// To open a file when walking requests it.
//
// It uses the cache system.
func (f *cachedFS) Open(name string) (fs.File, error) {
	name = path.Clean(f.relativePath + string(os.PathSeparator) + name)

	v := cachedFSFile{fs: f}
	var err error
	if f.cacheEnabled == true {
		v.openCacheValue, err = f.openCache.Get(name)
	} else {
		v.openCacheValue, err = f.openCache.Do(name)
	}
	if err != nil && errors.Is(err, NotStoredError) == false {
		logs.Verbose(`unable to open file"`, name, `": `, err.Error())
		return nil, &fs.PathError{Op: "open", Path: name, Err: err}
	}

	return &v, nil
}

// To read directory informations when walking requests it.
//
// It uses the cache system.
// If an error occurs, the directory is skipped.
func (f *cachedFS) ReadDir(name string) ([]fs.DirEntry, error) {
	name = path.Clean(f.relativePath + string(os.PathSeparator) + name)

	v := cachedFSFile{}
	var err error
	if f.cacheEnabled == true {
		v.openCacheValue, err = f.openCache.Get(name)
	} else {
		v.openCacheValue, err = f.openCache.Do(name)
		defer f.openCache.Undo(v.openCacheValue)
	}
	if err != nil {
		logs.Verbose(`unable to read directory "`, name, `": `, err.Error())
		return nil, fs.SkipDir
	}
	return v.openCacheValue.DirectoryInfos, nil
}

// To stat a cached file when walking requests it.
//
// It uses the cache system.
func (f *cachedFSFile) Stat() (fs.FileInfo, error) {
	return f.openCacheValue.FileInfo, nil
}

// To read a cached file when walking requests it.
//
// It uses the cache system.
func (f *cachedFSFile) Read(b []byte) (int, error) {
	return f.openCacheValue.File.Read(b)
}

// To close a cached file when walking requests it.
//
// It uses the cache system.
func (f *cachedFSFile) Close() error {
	if f.fs.cacheEnabled == false {
		return f.fs.openCache.Undo(f.openCacheValue)
	}
	return nil
}
