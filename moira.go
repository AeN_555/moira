// Package moira is a binary to serve HTTP requests to list and download server files.
//
// The server provides a list of all available files and create a hyperlink for each one.
// Each link allows the users to download the file.
package main

import (
	"io"
	"net/http"

	"gitlab.com/AeN_555/moira/handler"
	"gitlab.com/AeN_555/moira/logs"
	"gitlab.com/AeN_555/moira/security"
	"gitlab.com/AeN_555/moira/settings"
)

// To define the entry point of the binary.
func main() {
	settings.ParseFlags()
	logs.Configure(settings.Color, settings.Verbose)
	logs.Information(`server shares path "` + settings.PathToSharedDirectory + `" on port ` + settings.Port)

	h := handler.NewHandler()
	http.Handle(`/`, h)
	http.HandleFunc(`/favicon.ico`, getEmpty) // "favicon.ico" file can be implicitly requested

	var err error
	if settings.Security == true {
		priv, pub := security.KeysPaths()
		err = http.ListenAndServeTLS(`:`+settings.Port, pub, priv, nil)
	} else {
		err = http.ListenAndServe(`:`+settings.Port, nil)
	}
	if err != nil {
		logs.Error(err, `cannot start server`)
	}

	h.Clear()
}

// To send an empty response.
//
// It send an empty string to accept and discard the request.
func getEmpty(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "")
}
